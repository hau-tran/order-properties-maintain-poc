package com.axonivy.jaxrs.fin;

import static com.axonivy.jaxrs.fin.orderintegritycheck.OrderIntegrityCheckService.OrderIntegrityChecker.setOneTo;
import static com.axonivy.jaxrs.fin.orderintegritycheck.OrderIntegrityCheckService.OrderIntegrityChecker.setTwoTo;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.axonivy.jaxrs.fin.events.Action;
import com.axonivy.jaxrs.fin.events.InvoiceChangeEvent;
import com.axonivy.jaxrs.fin.events.When;
import com.axonivy.jaxrs.fin.logging.DEBUG;
import com.axonivy.jaxrs.fin.orderintegritycheck.OrderIntegrityCheckService;

@RequestScoped
public class InvoiceChangeObserver {

	@Inject
	private OrderIntegrityCheckService orderIntegrityCheck;
	
	/**
	 * This observing method will only be triggered if an Event of
	 * When(DELETE) fires with an InvoiceChangeEvent.
	 * 
	 */
	public void onInvoiceDeleted(@Observes @When(Action.DELETE) InvoiceChangeEvent deletedInvoice) {
		
		DEBUG.in(this)
			.log(() -> String.format("Reaction on Invoice [%s] got deleted", deletedInvoice.invoice().get()));
			
		orderIntegrityCheck
			.onlyDo(
					setOneTo(true),
					setTwoTo(false))
			.check(deletedInvoice.invoice().get());
	}
	
	
}
