package com.axonivy.jaxrs.fin;

import static com.axonivy.jaxrs.fin.events.Action.DELETE;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.axonivy.jaxrs.base.UsedOnlyByCdiOrTesting;
import com.axonivy.jaxrs.fin.events.FinEventChannel;
import com.axonivy.jaxrs.fin.events.InvoiceChangeEvent;
import com.axonivy.jaxrs.fin.logging.DEBUG;

@RequestScoped
public class InvoiceService {

	private FinEventChannel event;
	
	@Inject
	public InvoiceService(FinEventChannel event) {
		this.event = event;
	}
	
	@UsedOnlyByCdiOrTesting
	InvoiceService() {
	}
	
	public void deleteInvoice(long invoiceId) {
		Invoice invoice = new Invoice();
		invoice.setId(invoiceId);
		
		event.invoice(DELETE)
			.fire(new InvoiceChangeEvent(() -> invoice));
		
		DEBUG.in(this)
			.log(() -> "Invoice after processed by observers " + invoice);
	}
	
}
