package com.axonivy.jaxrs.fin;

import java.text.MessageFormat;

public class Invoice extends Order {
	
	@Override
	public String toString() {
		return MessageFormat.format("Invoice {0} ({1})", getId(), flagsAsString());
	}

	private String flagsAsString() {
		return String.format("%s|%s|%s|%s", isOne(), isTwo(), isThree(), isFour());
	}

}
