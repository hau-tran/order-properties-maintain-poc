package com.axonivy.jaxrs.fin.events;

import java.util.function.Supplier;

import com.axonivy.jaxrs.fin.Invoice;

public class InvoiceChangeEvent extends OrderChangeEvent {
	
	private Supplier<Invoice> invoice;
	
	/*
	 * Uses of `Supplier` in case the `Observers` never
	 * really read the the event's payload.
	 */
	public InvoiceChangeEvent(Supplier<Invoice> invoice) {
		this.invoice = invoice; 
	}
	
	public Supplier<Invoice> invoice() {
		return invoice;
	}

}
