package com.axonivy.jaxrs.fin.events;

public enum Action {
	CREATE, 
	SENT,
	DELIVERED,
	CANCELLED,
	DELETE;
}
