package com.axonivy.jaxrs.fin.events;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;

@RequestScoped
public class FinEventChannel {

	private Event<OrderChangeEvent> event;
	
	@Inject
	public FinEventChannel(Event<OrderChangeEvent> event) {
		this.event = event;
	}
	
	/**
	 * Only used by CDI
	 */
	FinEventChannel() {
	}
	
	
	public Event<InvoiceChangeEvent> invoice(Action action) {
		return event.select(InvoiceChangeEvent.class, new WhenQualifier(action));
	}
	
	/**
	 * <p>
	 * We need to provide a concrete instance of `When` to match
	 * with the `Observers`.
	 * </p>
	 * <p>
	 * Inspired by http://www.next-presso.com/2014/06/you-think-you-know-everything-about-cdi-events-think-again/
	 * </p>
	 */
	@SuppressWarnings("all")
	public class WhenQualifier extends AnnotationLiteral<When> implements When {

		private static final long serialVersionUID = -4887358121539307975L;
		
		private final Action action;
		
		public WhenQualifier(Action action) {
			this.action = action;
		}
		
		@Override
		public Action value() {
			return action;
		}
		
	}
	
}
