package com.axonivy.jaxrs.fin.orderintegritycheck;

import java.util.Collections;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

import com.axonivy.jaxrs.fin.orderintegritycheck.OrderIntegrityCheckService.OrderIntegrityChecker;

@RequestScoped
public class DefaultOrderIntegrityCheckerProducer {

	@Produces
	public List<OrderIntegrityChecker> produceDefaultCheckers() {
		return Collections.emptyList();
	}
	
}
