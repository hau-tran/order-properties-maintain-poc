package com.axonivy.jaxrs.fin.orderintegritycheck;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.axonivy.jaxrs.base.UsedOnlyByCdiOrTesting;
import com.axonivy.jaxrs.fin.Order;

@RequestScoped
public class OrderIntegrityCheckService {
	
	@FunctionalInterface
	public interface OrderIntegrityChecker {
		
		public static OrderIntegrityChecker setOneTo(boolean expetecedValue) {
			return order -> order.setOne(expetecedValue);
		}
		
		public static OrderIntegrityChecker setTwoTo(boolean expectedValue) {
			return order -> order.setTwo(expectedValue);
		}
		
		public void check(Order order);
	}

	private List<OrderIntegrityChecker> checkers = Collections.emptyList();
	
	@Inject
	public OrderIntegrityCheckService(List<OrderIntegrityChecker> checkers) {
		this.checkers = checkers;
	}
	
	@UsedOnlyByCdiOrTesting
	OrderIntegrityCheckService(){
	}

	/**
	 * Applies all {@code OrderIntegrityCheckers} to given order
	 */
	public void check(Order order) {
		checkers.forEach(c -> c.check(order));
	}
	
	/**
	 * Overrides all prior existing {@code OrderIntegrityCheckers} with the given ones.
	 */
	public OrderIntegrityCheckService onlyDo(OrderIntegrityChecker... checkers) {
		this.checkers = Arrays.asList(checkers);
		return this;
	}
	
	
}
