package com.axonivy.jaxrs.fin.logging;

import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DEBUG {

	private final Level DEBUG_TO_LOG_LEVEL = Level.INFO;

	private Logger logger;
	
	public static DEBUG get() {
		return new DEBUG(Logger.getLogger("DEBUG"));
	}
	
	public static DEBUG in(Class<?> category) {
		return new DEBUG(Logger.getLogger(category.getName()));
	}
	
	public static DEBUG in(Object instance) {
		return new DEBUG(Logger.getLogger(instance.getClass().getName()));
	}
	
	private DEBUG(Logger logger) {
		this.logger = logger;
	}
	
	public void log(Supplier<String> log) {
		logger.log(DEBUG_TO_LOG_LEVEL, log);
	}
	
}
