package com.axonivy.jaxrs.fin;

public class Order {

	private long id;
	private boolean one;
	private boolean two;
	private boolean three;
	private boolean four;

	public boolean isOne() {
		return one;
	}

	public void setOne(boolean one) {
		this.one = one;
	}

	public boolean isTwo() {
		return two;
	}

	public void setTwo(boolean two) {
		this.two = two;
	}

	public boolean isThree() {
		return three;
	}

	public void setThree(boolean three) {
		this.three = three;
	}

	public boolean isFour() {
		return four;
	}

	public void setFour(boolean four) {
		this.four = four;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
