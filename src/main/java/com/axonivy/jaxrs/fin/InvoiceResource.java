package com.axonivy.jaxrs.fin;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("invoices")
@RequestScoped
public class InvoiceResource {

	@Inject
	private InvoiceService invoiceService;
	
	
	@Path("{invoice-id}")
	@DELETE
	public void deleteInvoice(@PathParam("invoice-id") long invoiceId) {
		invoiceService.deleteInvoice(invoiceId);
	}
	
}
