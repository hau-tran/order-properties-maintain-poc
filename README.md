# README #

### Intro

This project serves as a PoC of using CDI `Events` to solve the problem of handling luzfin's `Order`'s properties.


### Details

There are two things differently with the current implementation of luzfin:

**The services doesn't directly invoke `OrderService`. Instead, they have to communicate via a channel `FinEventChannel`.**

- The code to execute could be separated into `Observers` instead of putting all into `OrderService`. The `Observers` can be normal CDI components.
- Please note that the Observers are executed **synchronously**, thus they are contained in one transaction.

**The job of maintaining `Order`'s properties is delegated to a class `OrderIntegrityCheckService`.**

- This class contains an `interface OrderIntegrityChecker` for the other to implements.
- The idea is, there could be a `DefaultOrderIntegrityCheckerProducer` to produce a `List<OrderIntegrityChecker>` as **default** ones.
- Client of `OrderIntegrityCheckService` could use the methods of its API to manipulate to choose whether the default checkers are used or override them by supplying others.